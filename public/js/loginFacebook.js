/*
 * Login y registro Facebook
 */

document.getElementById("token_fb").value = "";
document.getElementById("id_fb").value = "";
document.getElementById("email_fb").value = "";
document.getElementById("name_fb").value = "";
document.getElementById("status").innerHTML = "";

document.getElementById("buttonfacebook").addEventListener("click", fbLogin);
document.getElementById("buttonfacebookregister").addEventListener("click", fbReg);

window.fbAsyncInit = function() {
    FB.init({
      appId      : '595047381231598',
      xfbml      : true,
      version    : 'v2.12'
    });
console.log(FB);
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            document.getElementById('status').innerHTML = 'User logged in';
            document.getElementById("token_fb").value = response.authResponse.accessToken;
            getFbUserData();
            

        } else if (response.status === 'not_authorized') {
            document.getElementById('status').innerHTML = 'Not authorized for login';
        } else {
            document.getElementById('status').innerHTML = 'You is not login';
        }
    });
};

// Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            document.getElementById('status').innerHTML = 'User logged in';
            document.getElementById("token_fb").value = response.authResponse.accessToken;
            getFbUserData();
            document.getElementById("form_facebook").submit();
            // if (response.status === 'connected') {
            //     document.getElementById('status').innerHTML = 'User logged in';
            //     document.getElementById("token_fb").value = response.authResponse.accessToken;
            //     getFbUserData();
                
            //     FB.getLoginStatus(function(response) {
            //         if (response.status === 'connected') {
            //             document.getElementById('status').innerHTML = 'User logged in';
            //             document.getElementById("token_fb").value = response.authResponse.accessToken;
            //             getFbUserData();
            //         }
            //     });
            // }

            if(document.getElementById("id_fb").value != "" || document.getElementById("id_fb").value == undefined ){
                document.getElementById("form_facebook").submit();
            }
        } else {
            document.getElementById('status').innerHTML = 'Not authorized for login';
        }

    }, {scope: 'email'});
}

function fbReg() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            document.getElementById('status_r').innerHTML = 'User logged in';
            document.getElementById("token_fb_r").value = response.authResponse.accessToken;
            getFbUserDataR();
            
            // if (response.status === 'connected') {
            //     document.getElementById('status').innerHTML = 'User logged in';
            //     document.getElementById("token_fb").value = response.authResponse.accessToken;
            //     getFbUserData();
                
            //     FB.getLoginStatus(function(response) {
            //         if (response.status === 'connected') {
            //             document.getElementById('status').innerHTML = 'User logged in';
            //             document.getElementById("token_fb").value = response.authResponse.accessToken;
            //             getFbUserData();
            //         }
            //     });
            // }

        } else {
            document.getElementById('status_r').innerHTML = 'Not authorized for login';
        }

    }, {scope: 'email'});
}

function getFbUserData(){
    FB.api('/me', { locale: 'en_US', fields: 'id,name,first_name,last_name,email,link,gender,locale,picture' },
        function(response) {
console.log("Respuesta facebbook" + response);
            document.getElementById("id_fb").value = response.id;
            document.getElementById("name_fb").value = response.name;
            document.getElementById("email_fb").value = response.email;
            document.getElementById("img_cliente").value = response.picture.data.url;
            // document.getElementById("...").value = response.first_name;
            // document.getElementById("...").value = response.last_name;
            // document.getElementById("...").value = response.gender;
            // document.getElementById("...").value = response.locale;
            // document.getElementById("...").value = response.picture.data.url;
            // document.getElementById("...").value = response.link;
        }
    );
}

function getFbUserDataR(){
    FB.api('/me', { locale: 'en_US', fields: 'id,name,first_name,last_name,email,link,gender,locale,picture' },
        function(response) {
console.log("Respuesta facebbook" + response);
            document.getElementById("id_fb_r").value = response.id;
            document.getElementById("name_fb_r").value = response.name;
            document.getElementById("email_fb_r").value = response.email;
            document.getElementById("img_cliente_r").value = response.picture.data.url;
            // document.getElementById("...").value = response.first_name;
            // document.getElementById("...").value = response.last_name;
            // document.getElementById("...").value = response.gender;
            // document.getElementById("...").value = response.locale;
            // document.getElementById("...").value = response.picture.data.url;
            // document.getElementById("...").value = response.link;
            document.getElementById("form_facebook_r").submit();
        }
    );
}