//Login y register google

document.getElementById("id_goo").value = "";
document.getElementById("name_goo").value = "";
document.getElementById("imageurl_goo").value = "";
document.getElementById("email_goo").value = "";
document.getElementById("token_goo").value = "";

var googleUser = {};

var startAppL = function() {
  
  gapi.load('auth2', function(){
    // Retrieve the singleton for the GoogleAuth library and set up the client.
    auth2 = gapi.auth2.init({
      client_id: '987905943046-gcugreao7aep4ojripu0kluji7b7fq7h.apps.googleusercontent.com',
      cookiepolicy: 'single_host_origin',
      // Request scopes in addition to 'profile' and 'email'
      //scope: 'additional_scope'
    });
    attachSignin(document.getElementById('buttongooglelogin'));
    attachSignin(document.getElementById('buttongoogleregister'));
  });
};

function attachSignin(element) {
  // console.log(element.id);
  auth2.attachClickHandler(element, {},
      function(googleUser) {
        var profile = googleUser.getBasicProfile();
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
        if(id_token){
            document.getElementById("id_goo").value = profile.getId();
            document.getElementById("name_goo").value = profile.getName();
            document.getElementById("imageurl_goo").value = profile.getImageUrl();
            document.getElementById("email_goo").value = profile.getEmail();
            document.getElementById("token_goo").value = googleUser.getAuthResponse().id_token;
            // localStorage.setItem("loginGoogle", true);
            document.getElementById("form_googlelogin").submit();
        }
      }, function(error) {
        // alert(JSON.stringify(error, undefined, 2));
      });
}