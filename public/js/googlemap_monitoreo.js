var autocomplete = document.getElementById('txt_Direccion');
var latitude = document.getElementById('latitud');
var longitude = document.getElementById('longitud');
var coordinates = document.getElementById('coords');
var geocoder;
var map_int;
var map_act;
var markers = [];
var markers_d = [];
var markers_p = [];
var arr_geocercas = [];
var arr_Data = [];
var coord_datia = {lat: 4.679078, lng: -74.048328}; 
var posicion_act = {lat: 4.68111, lng: -74.04777};
function initMap2() {
  var parque93 = {lat: 4.6767703, lng: -74.0483488};
  
  var map_int = new google.maps.Map(
    document.getElementById('map_puntos'), 
    {zoom:15, center: parque93,}
  );
}

function declaracion_Main(){
  //geocoder = new google.maps.Geocoder();
  autocomplete = document.getElementById('txt_Direccion');
  var searchautocomplete = new google.maps.places.Autocomplete(autocomplete);
  searchautocomplete.addListener('place_changed', function(){

  var place = searchautocomplete.getPlace();
  latitude.value = 0;
  longitude.value = 0;
  coordinates.value = 0;
  if (!place.geometry.viewport){
    window.alert("Error al buscar el lugar");
    return;
  }

  latitude.value = place.geometry.location.lat();
  longitude.value = place.geometry.location.lng();
  coordinates.value = place.geometry.location;
  autocomplete_int = document.getElementById('txt_Direccion');
  establecer_direccion_session(autocomplete_int.value,latitude.value,longitude.value);

  });

}

function monitoreo_maps(){
  geocoder = new google.maps.Geocoder();
	var infoWindow = new google.maps.InfoWindow;
  var searchautocomplete = new google.maps.places.Autocomplete(autocomplete);
  
  console.log('Mostrando direcciones');
    if (typeof map_int == "undefined"){
      map_int = new google.maps.Map(
        document.getElementById(map_act), 
        {zoom:17, center: coord_monitoreo,}
      );
    }

        var marker = new google.maps.Marker({
        position: coord_monitoreo,
        map: map_int,
        icon: 'https://storage.googleapis.com/datia-files/hom/assets/iconos_consola_index/assets_nuevos/MisDirecciones01.png',
        });
        markers.push(marker);
        //markers[a].setMap(map_int);
      
}

function establecer_direccion_session(var_Direccion, var_Lat, var_Lng){
  var urlActual = window.location.protocol + '//' + window.location.host + '/';
  var url = urlActual+"direcciones/establecer_direccion_session";
  $.ajax({
      url: url,
      data: {"txt_Direccion": var_Direccion, "txt_Lat": var_Lat, "txt_Lng": var_Lng},
      type : 'POST',
      success: function(respuesta) {

        //alert("txt_Direccion" + var_Direccion + "txt_Lat" + var_Lat + "txt_Lng" + var_Lng);
// MOSTAR SELECCIONADA
//alert(respuesta);
            if (respuesta){
                //location.reload(true);
                //alert("Se establecio dirección con éxito!!! (" + var_Direccion + ")");
                console.log("Se establecio dirección con éxito!!! (" + var_Direccion + ")");
            }else{
                //alert("No se pudo establecer dirección. " + respuesta);
                console.log("No se pudo establecer dirección. " + respuesta);
            }
        },
        error: function() {
          console.log("No se ha podido obtener la información");
        }
   });
}

function initMapDirecciones() {
  geocoder = new google.maps.Geocoder();
	var infoWindow = new google.maps.InfoWindow;
	var searchautocomplete = new google.maps.places.Autocomplete(autocomplete);
	
	MostrarDirecciones(arr_Data);
    //searchautocomplete.bindTo("bounds",map);
 
   	
}

function eliminaMarcadores(array_marcadores)
  { 
    console.log('Borrando todos los marcadores');
    for(a in array_marcadores)
    {
      array_marcadores[a].setMap(null);
    }
    array_marcadores = [];
  }

function MostrarDirecciones(arry_direcciones)
  { 
    console.log('Mostrando direcciones');
    if (typeof map_int == "undefined"){
      map_int = new google.maps.Map(
        document.getElementById(map_act), 
        {zoom:17, center: coord_datia,}
      );
    }

      for(a in arry_direcciones)
      {
console.log(typeof map_int + " Item map: " + arry_direcciones[a]['lat'] + ' - ' + arry_direcciones[a]['lng']);
        var latlng = {lat: parseFloat(arry_direcciones[a]['lat']), lng: parseFloat(arry_direcciones[a]['lng'])};
        var marker = new google.maps.Marker({
        position: latlng,
        map: map_int,
        icon: 'https://storage.googleapis.com/datia-files/hom/assets/iconos_consola_index/assets_nuevos/MisDirecciones01.png',
        });
        markers.push(marker);
        if (a == 0) {
          map_int.setCenter(latlng);
        }
        //markers[a].setMap(map_int);
      }
    
  }

function estoyAqui() {

	var infoWindow = new google.maps.InfoWindow;
	var searchautocomplete = new google.maps.places.Autocomplete(autocomplete);
	var parque93 = {lat: 4.6767703, lng: -74.0483488};

	var map = new google.maps.Map(
		document.getElementById('google_map'),
		{zoom:15, center: parque93,}
		);

	var marker = new google.maps.Marker({
	    position: parque93,
	    map: map,
	    icon: '../img/Direcciones02.png',
	});

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
			  lat: position.coords.latitude,
			  lng: position.coords.longitude
			};			

			marker.setPosition(pos);
			latitude.value = pos.lat;
   			longitude.value = pos.lng;
   			coordinates.value = [pos.lat, pos.lng];
			infoWindow.setPosition(pos);
			infoWindow.setContent('Usted se encuentra aqui');
			infoWindow.open(map);
			map.setCenter(pos);
		}, 
		function() {
		handleLocationError(true, infoWindow, map.getCenter());
		});
    } else {
      handleLocationError(false, infoWindow, map.getCenter());
    }

    searchautocomplete.bindTo("bounds",map);
 
   	searchautocomplete.addListener('place_changed', function(){

   		var place = searchautocomplete.getPlace();

   		if (!place.geometry.viewport){
   			window.alert("Error al mostrar el lugar");
   			return;
   		}

   		if (place.geometry.viewport){
   			map.fitBounds(place.geometry.viewport);
   			map.setZoom(15);
   		}else{
   			map.setCenter(place.geometry.location);
   			map.setZoom(15);
   		}

   		marker.setPosition(place.geometry.location);
   		latitude.value = place.geometry.location.lat();
   		longitude.value = place.geometry.location.lng();
   		coordinates.value = place.geometry.location;
   		map.setCenter(marker.getPosition());

   	});

    map.addListener('click', function(e) {
   		marker.setPosition(e.latLng);
   		latitude.value = e.latLng.lat();
   		longitude.value = e.latLng.lng();
   		coordinates.value = e.latLng.toString();
   		map.setCenter(marker.getPosition());
  	});

}



function MostrarGeoCerca(var_Coordenadas, var_Zoom, var_Color_Borde, var_Color_Relleno) {
console.log(typeof map_int);
  if (typeof map_int == "undefined"){

    map_int = new google.maps.Map(
      document.getElementById('google_map'),
      {zoom:var_Zoom, center: posicion_act, mapTypeControl: false}
      );
  if ("geolocation" in navigator){ //check Geolocation available 
        console.log("Geolocalización Disponible!");
        navigator.geolocation.getCurrentPosition(function(position){ 
              posicion_act = {lat: position.coords.latitude, lng: position.coords.longitude};
              console.log("Posicion: " + posicion_act);
              var input = $("#address");
              var latitud = $("#latitud");
              var longitud = $("#longitud");
              var coords = $("#coords");
              var var_coords = '(' + position.coords.latitude + ',' + position.coords.longitude + ')';

              geocoder.geocode({'location': posicion_act}, function(results, status) {
              if (status === 'OK') {
                if (results[0]) {
                  //map.setZoom(20);
                  map_int.setCenter(posicion_act);
                  input.val(results[0].formatted_address);
                  latitud.val(position.coords.latitude);
                  longitud.val(position.coords.longitude);
                  coords.val(var_coords);
                  var marker = new google.maps.Marker({
                    position: posicion_act,
                    map: map_int
                  });
                  markers.push(marker);
                  //infowindow.open(map, marker);
                } else {
                  window.alert('Sin resultados válidos');
                }
              } else {
                window.alert('Localización ha fallado dirección irreconocible');
              }
            });
          });

    }else{
        console.log("Geolocalización no Disponible!");
    }
  }
  
   var infoWindow = new google.maps.InfoWindow;
   var searchautocomplete = new google.maps.places.Autocomplete(autocomplete);
   

   //añadir todas las geocercas

   var GeoCerca = new google.maps.Polygon({
          paths: var_Coordenadas,
          strokeColor: var_Color_Borde, //borde
          strokeOpacity: 0.5,
          strokeWeight: 2,
          fillColor: var_Color_Relleno, //relleno
          fillOpacity: 0.15,
          label: 'Zona Prueba 1'
        });
        GeoCerca.setMap(map_int);

   //fin de ciclo     
        GeoCerca.addListener('click', showArrays);
        arr_geocercas.push(GeoCerca);
}

function showArrays(event) {
        // Since this polygon has only one path, we can call getPath() to return the
        // MVCArray of LatLngs.
        
        var input = $("#address");
        var latitud = $("#latitud");
        var longitud = $("#longitud");
        var coords = $("#coords");
        //var resultado = _autofillFromUserLocation(event.latLng);
        var latlng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
        var var_coords = '(' + event.latLng.lat() + ',' + event.latLng.lng() + ')';
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
              //map.setZoom(20);
              map.setCenter(latlng);
              setMapOnAll(null);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              markers.push(marker);
              input.val(results[0].formatted_address);
              latitud.val(event.latLng.lat());
              longitud.val(event.latLng.lng());
              coords.val(var_coords);
              //infowindow.open(map, marker);
            } else {
              window.alert('Sin resultados válidos');
            }
          } else {
            window.alert('Localización ha fallado dirección irreconocible');
          }
        });

//alert(event.latLng.lat() + ',' + event.latLng.lng() + ' - ' + place);
        

        //infoWindow.open(map);
      }

      var _autofillFromUserLocation = function (latLng) {
        _reverseGeocode(latLng, function (result) {
            $('#address').each(function (i, fieldset) {
                _updateAddress({
                    fieldset: fieldset,
                    address_components: result.address_components
                });
            });
        });
    };

    var _reverseGeocode = function (latLng, successCallback, failureCallback) {
      var _geocoder = new google.maps.Geocoder();
        _geocoder.geocode({ 'location': latLng }, function(results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    successCallback(results[1]);
                } else {
                    if (failureCallback)
                        failureCallback(status);
                }
            } else {
                if (failureCallback)
                    failureCallback(status);
            }
        });
    };

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

function setMapOnAllD(map) {
  for (var i = 0; i < markers_d.length; i++) {
    markers_d[i].setMap(map);
  }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
	                      'Error: The Geolocation service failed.' :
	                      'Error: Your browser doesn\'t support geolocation.');
	infoWindow.open(map);
}




function MostrarTraking(var_Coordenadas, var_Zoom, var_PM) {
  map = new google.maps.Map(
      document.getElementById('google_map_tracking'),
      {zoom:var_Zoom, center: var_Coordenadas, mapTypeControl: false}
      );
      /*var marker_p = new google.maps.Marker({
            position: var_PM,
            map: map,
            icon: '../img/Logo02.png'
          });*/
      
      var marker = new google.maps.Marker({
            position: var_Coordenadas,
            map: map
          });
          markers.push(marker);

      
          
       
            //posicion_act = {lat: position.coords.latitude, lng: position.coords.longitude};
   
   //añadir todas las geocercas
  }

  

      