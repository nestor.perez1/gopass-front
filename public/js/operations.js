// Hover buttons

$(".b1").hover(
  function() {
    $(this).children("a").children("div").children("img").attr('src','../img/BotonAsignar02.svg');
    $(this).children("a").children("div").children("p").css('visibility','visible');
  }, function() {
    $(this).children("a").children("div").children("img").attr('src','../img/BotonAsignar.svg');
    $(this).children("a").children("div").children("p").css('visibility','hidden');
  }
);

$(".b2").hover(
  function() {
    $(this).children("a").children("div").children("img").attr('src','../img/BotonEditar02.svg');
    $(this).children("a").children("div").children("p").css('visibility','visible');
  }, function() {
    $(this).children("a").children("div").children("img").attr('src','../img/BotonEditar.svg');
    $(this).children("a").children("div").children("p").css('visibility','hidden');
  }
);

$(".b3").hover(
  function() {
    $(this).children("a").children("div").children("img").attr('src','../img/BotonCancelar02.svg');
    $(this).children("a").children("div").children("p").css('visibility','visible');
  }, function() {
    $(this).children("a").children("div").children("img").attr('src','../img/BotonCancelar.svg');
    $(this).children("a").children("div").children("p").css('visibility','hidden');
  }
);

$(".b4").hover(
  function() {
    $(this).children("a").children("div").children("img").attr('src','../img/BotonDetalle02.svg');
    $(this).children("a").children("div").children("p").css('visibility','visible');
  }, function() {
    $(this).children("a").children("div").children("img").attr('src','../img/BotonDetalle.svg');
    $(this).children("a").children("div").children("p").css('visibility','hidden');
  }
);