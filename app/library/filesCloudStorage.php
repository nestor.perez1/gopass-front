<?php

    namespace Google\Cloud\Samples\Storage;

    require __DIR__ . '/../../vendor/autoload.php';

    use Google\Cloud\Storage\StorageClient;

    class ServiceCloud{

        function auth_cloud_explicit($projectId, $serviceAccountPath)
            {
            $config = [
                'keyFilePath' => $serviceAccountPath,
                'projectId' => $projectId,
            ];
            return new StorageClient($config);
        }

        function upload_object($bucketName, $objectName, $tmp_name)
        {
            $config = [
                'keyFilePath' => __DIR__."/../../gcp.json",
                'projectId' => "curious-truth-161716",
            ];
            //$storage = auth_cloud_explicit("curious-truth-161716",__DIR__."/../../gcp.json");
            $storage = new StorageClient($config);
            $file = fopen($tmp_name, 'r');
            $bucket = $storage->bucket($bucketName);
            $object = $bucket->upload($file, [
                'name' => $objectName
            ]);
            $url = "https://storage.googleapis.com/".$bucketName."/".$objectName;
            return $url;
        }
    }