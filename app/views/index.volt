<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<!--estilos-->
{{ get_title() }}


{{ stylesheet_link('bootstrap-4.3.1-dist/bootstrap.min.css') }}

{{ stylesheet_link('/css/bootstrap-theme.css.map') }}
<!--{{ stylesheet_link('css/bootstrap-theme.min.css') }}
{{ stylesheet_link('/css/bootstrap.css.map') }}
{{ stylesheet_link('/css/component.css') }}-->
<!---->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your invoices">
<meta name="author" content="Phalcon Team">
{{ javascript_include('/js/jquery-3.2.1.min.js') }}
</head>
<body>
<!--estilos invo responsive-->
{{ content() }}

</body>


</html>