<?php

use Phalcon\Mvc\Controller;
include __DIR__ . '/../library/class.IPInfoDB.php';

class ControllerBase extends Controller
{
	protected $serverapi;
    protected $key = "";
    protected $platform;
    const URLMAIL = __DIR__ . '/../library/PHPMailer/PHPMailerAutoload.php';
    const URLMAILCONFIG = __DIR__ . '/../library/PHPMailer/phpmailer_config.php';    
    const URLPDF = __DIR__ . '/../library/dompdf/autoload.inc.php';
    const URLS3 = __DIR__ . '/../library/s3_config.php';
    const KEYHASH = '4960c1ba19773c1302a910e51fe14911';

    /**
    * @desc - array con los lenguajes disponibles en la app
    */
    protected $_avalaibleLangs = array("es","en","esBol","enBol");
    protected $t;
     
    /**
    * @desc - cargamos la traducción correspondiente
    */
    protected function initialize()
    {

        $this->serverapi = $this->config->application->baseUriApi;
        $this->view->baseUri = $this->config->application->baseUri;
        $this->recipient = "info@datia.co";
        $this->tag->prependTitle('Gopass | ');
        $this->view->setTemplateAfter('main');
        $this->getTranslation();
        

//echo print_r($this->view->hom_data);die;

    }

    function get_hom_data(){
        $url = $this->serverapi . "geo/webServiciosIndex";
        $post = $this->request->getPost();
        $data = array(
            "key_hash" => $this->keyhash,
        );
        //echo $url . " - " . json_encode($data);die;
        $answer = $this->sendRedirect($url, $data);
        //echo $url . " - " . print_r($answer);die;
        $answer = json_decode($answer);
        return $answer;
        //echo $url . " - " . print_r($answer);die; 
        /* if (isset($answer->return) && $answer->return) {
            $this->view->servicios   = $answer->dato01;
            $this->view->promociones = $answer->dato02;
            $this->view->premium     = $answer->dato03;
            $this->view->para_ti     = $answer->dato04;

        } */
    }


    /**
    * @desc - obtenemos las traducciones por la sesión del usuario, por defecto español
    */
    public function getTranslation()
    {
     //obtenemos el archivo con las traducciones
     require "../app/messages/".($this->session->has("language") ? $this->session->get("language") : 'es') .".php";
     
     //devolvemos el objeto con las traducciones del idioma escogido
     $translate = new Phalcon\Translate\Adapter\NativeArray(array(
        "content" => $messages
     ));
     
     //establece la variable $t disponible en todas las vistas
     $this->view->t = $translate;
     $this->t = $translate;
    }


   //Magic Para los servicios
    public function sendRedirect($url, $data)
    {
// echo "Entro redirect";die;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $serverOutput = curl_exec ($ch);
        curl_close ($ch);

//echo "Entro redirect" .  print_r($serverOutput) ;die;
 
        return $serverOutput;
    }

    function noficaciones(){
//echo ('kasdjdjasñ'); die;
        $id_user = $this->view->auth['id_user'];
        $url = $this->serverapi."geo/misNotificaciones";
//echo $id_user;die;
        $data = array(
            "key_hash" => $this->keyhash,
            "id_user" =>  $id_user,
        );
//echo $data; die;
        $notifi = $this->sendRedirect($url,$data);
//echo print_r($notifi);die;
        $notifi = json_decode($notifi);
//echo print_r($notifi);die;
        if(isset($notifi->return) && $notifi->return) {
                   $this->view->notificacion = $notifi->data01;
//echo  $this->view->notificacion; die;
        }
    }

    public function url_base($forwarded_host = false) {
        $ssl   = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
        $proto = strtolower($_SERVER['SERVER_PROTOCOL']);
        $proto = substr($proto, 0, strpos($proto, '/')) . ($ssl ? 's' : '' );
        if ($forwarded_host && isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
            $host = $_SERVER['HTTP_X_FORWARDED_HOST'];
        } else {
            if (isset($_SERVER['HTTP_HOST'])) {
                $host = $_SERVER['HTTP_HOST'];
            } else {
                $port = $_SERVER['SERVER_PORT'];
                $port = ((!$ssl && $port=='80') || ($ssl && $port=='443' )) ? '' : ':' . $port;
                $host = $_SERVER['SERVER_NAME'] . $port;
            }
        }
        return $proto . '://' . $host;
    }

     public function detect_city($ip) {
        $ipinfodb = new IPInfoDB('b805a54df5db1040f55cf9ae574590af666a1104326bf1049a92cdc2b4000599');
        $result = json_encode($ipinfodb->getCity($ip));
        //echo print_r($result); die;
        return json_decode($result);
        
    } 
}
